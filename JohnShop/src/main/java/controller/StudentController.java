package controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import database.IStudentDatabase;
import entities.Student;
import exceptions.ResourceNotFoundException;

@RestController
@RequestMapping("/api/student")
public class StudentController {

	@Autowired
	private IStudentDatabase repo;
	
	@GetMapping("")
	public List<Student> getAll() {
		return repo.findAll(); 		
	}
	
	@PostMapping("/add")
	public Student create(@Valid @RequestBody Student data) {
		return repo.save(data);
	}
	
	@GetMapping("/{id}")
	public Student get(@PathVariable(value="id")  int id) {
		return repo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Student","id",id));
	}
	
	// Update a Note
	@PutMapping("/{id}")
	public Student update(@PathVariable(value = "id") int id,@Valid @RequestBody Student details) {
	
		Student student= repo.findById(id)
	            .orElseThrow(() -> new ResourceNotFoundException("Student", "id", id));				
		
		student.setName(details.getName());
		student.setLocation(details.getLocation());
	    	    
	    student = repo.save(student);
	    return student;
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") int id) {
		Student student = repo.findById(id)
	            .orElseThrow(() -> new ResourceNotFoundException("Student", "id", id));

	    repo.delete(student);

	    return ResponseEntity.ok().build();
	}
}
