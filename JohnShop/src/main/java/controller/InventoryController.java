package controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import database.IInventoryDatabase;
import entities.Inventory;
import exceptions.ResourceNotFoundException;

@RestController
@RequestMapping("/api/inventory")
public class InventoryController {

	@Autowired
	private IInventoryDatabase repo;
	
	@GetMapping("")
	public List<Inventory> getAll() {
		return repo.findAll(); 		
	}
	
	@PostMapping("")
	public Inventory create(@Valid @RequestBody Inventory data) {
		return repo.save(data);
	}
	
	@GetMapping("/{id}")
	public Inventory get(@PathVariable(value="id")  int id) {
		return repo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Inventory","id",id));
	}
	
	// Update a Note
	@PutMapping("/{id}")
	public Inventory update(@PathVariable(value = "id") int id,@Valid @RequestBody Inventory details) {
	
		Inventory inventory= repo.findById(id)
	            .orElseThrow(() -> new ResourceNotFoundException("Inventory", "id", id));				
				
	  
	    inventory.setItem(details.getItem());
	    inventory.setPrice(details.getPrice());
	    inventory.setType(details.getType());
	    inventory.setAvailable(details.getAvailable());
	    
	    inventory = repo.save(inventory);
	    return inventory;
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") int id) {
		Inventory inventory = repo.findById(id)
	            .orElseThrow(() -> new ResourceNotFoundException("Inventory", "id", id));

	    repo.delete(inventory);

	    return ResponseEntity.ok().build();
	}
}
