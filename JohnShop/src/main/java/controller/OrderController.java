package controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import exceptions.ResourceNotFoundException;
import database.IOrderDatabase;
import entities.Order;


@RestController
@RequestMapping("/api/order")
public class OrderController {

	@Autowired
	private IOrderDatabase repo;
	
	@GetMapping("")
	public List<Order> getAll() {
		return repo.findAll(); 		
	}
	
	@PostMapping("")
	public Order create(@Valid @RequestBody Order data) {
		return repo.save(data);
	}
	
	@GetMapping("/{id}")
	public Order get(@PathVariable(value="id")  int id) {
		return repo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Order","id",id));
	}
	
	// Update a Note
	@PutMapping("/{id}")
	public Order update(@PathVariable(value = "id") int id,@Valid @RequestBody Order details) {
	
		Order order = repo.findById(id)
	            .orElseThrow(() -> new ResourceNotFoundException("Order", "id", id));				
				
	    order.setItemName(details.getItemName());
	    order.setStudent(details.getStudent());
	    order.setAmtDue(details.getAmtDue());
	    	    
	    order = repo.save(order);
	    return order;
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") int id) {
		Order order = repo.findById(id)
	            .orElseThrow(() -> new ResourceNotFoundException("Order", "id", id));

	    repo.delete(order);

	    return ResponseEntity.ok().build();
	}
}
